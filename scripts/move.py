#!/usr/bin/env python
import rospy
from std_msgs.msg import Int16
from geometry_msgs.msg import Twist
import numpy as np

rospy.init_node('controller', anonymous=True)
pub = rospy.Publisher('command', Int16, queue_size=20)
rate = rospy.Rate(1) #10Hz


def callback(msg):
    talker(msg.angular.z)
    rospy.loginfo(rospy.get_caller_id() + "I heard %i", np.int16(1000*msg.angular.z))
    rate.sleep()


def talker(val):
    rospy.loginfo(val)
    pub.publish(val)
       

def listener(): 
    rospy.Subscriber("cmd_vel_mux/input/teleop", Twist, callback)
    rospy.spin()


if __name__ == '__main__':
    listener()
